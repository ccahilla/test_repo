'''
test_module_code.py

Uses pytest to run a unit test on the code in test_repo/module_code.py
Should be automatically run by gitlab CI.

Craig Cahillane
February 6th, 2021
'''

import test_repo

def test_always_passes():
    assert True

def test_add_two_numbers_ints():
    num1 = 4
    num2 = 5
    assert test_repo.add_two_numbers(num1, num2) == 9

def test_add_two_numbers_floats():
    num1 = 4.21
    num2 = 3.33
    total = 7.54
    epsilon = 1e-10
    assert abs(test_repo.add_two_numbers(num1, num2) - total) < epsilon

# def test_always_fails():
#     assert False

def test_add_two_numbers_strings():
    str1 = 'ab'
    str2 = 'ba'
    assert test_repo.add_two_numbers(str1, str2) == 'abba'
