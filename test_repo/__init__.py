'''
__init__.py
Required by python to be recognized as a python module.
Arranges the importing of the test_repo code
'''

from test_repo.module_code import *
from test_repo.version import __version__

NAME = 'test_repo'
VERSION = __version__
