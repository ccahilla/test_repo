'''
module_code.py

very basic example python module with functions we can call and test using pytest.
pylint can also be called on this code to see if it follows PEP8 convention.

Craig Cahillane
Feb 6, 2021
'''


def add_two_numbers(num1, num2):
    '''Code which adds two numbers together.
    Inputs
    ------
    num1: float. first number input
    num2: float. second number input

    Output
    ------
    value: float. sum of num1 and num2
    '''
    value = num1 + num2
    return value
