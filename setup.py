'''
Setup
'''

import setuptools
from distutils.util import convert_path

main_ns = {}
ver_path = convert_path('test_repo/version.py')
with open(ver_path) as ver_file:
    exec(ver_file.read(), main_ns)

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="test_repo",
    version=main_ns['__version__'],
    author="Craig Cahillane",
    author_email="ccahilla@gmail.com",
    description="test repository for pylint and pytest incorporated into gitlab CI",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ccahilla/test_repo/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
