import numpy as np 

# parameteres
board_size = 3
player_number = board_size**2
game_winners = np.zeros(player_number, dtype=int)

attack_win_prob = 0.5

number_of_games = 1000000
game_rounds = player_number - 1

verbose = False


for game_number in range(number_of_games):
    # print(f"game_number = {game_number}")

    # Play The Floor Game
    # Choose a random player 1 - 9
    # Have them attack another player to try to take their territory
    # Win probability = 50%
    # Try to figure out the best starting square by win percentage

    # Set up the board and players
    board = np.arange(1,player_number + 1).reshape((board_size, board_size))
    players = np.arange(1,player_number + 1)
    
    if verbose:
        print()
        print(board)
    
    # Cycle through 8 game rounds
    for round in range(game_rounds):

        # Randomly sample from remaining players
        attacker = np.random.choice(players)

        ### Find potential opponents
        defenders = np.array([], dtype=int)
    
        for jj in range(board_size):
            for kk in range(board_size):
                if board[jj,kk] == attacker:
                    # look around and add opponents
                    if not jj == 0:
                        defenders = np.append(defenders, board[jj-1,kk] )
                    if not jj == board_size - 1:
                        defenders = np.append(defenders, board[jj+1,kk] )
                    if not kk == 0:
                        defenders = np.append(defenders, board[jj,kk-1] )
                    if not kk == board_size - 1:
                        defenders = np.append(defenders, board[jj,kk+1] )

        # Remove duplicates and attacker themselves
        defenders = np.unique(defenders)
        defenders = defenders[defenders != attacker]

        if verbose:
            print()
            print(f"attacker = {attacker}")
            print(f"defenders = {defenders}")

        ### Choose a random direction to attack
        defender = np.random.choice(defenders)

        ### Attack
        attack = np.random.rand()

        # If attack > attack_win_prob, attacker wins
        if attack > attack_win_prob:
            winner = attacker
            loser = defender
        else: 
            winner = defender
            loser = attacker

        if verbose:
            print()
            print(f"Winner is {winner}")
            print(f"Loser is {loser}")
        
        # Losing player is eliminated
        players = players[players != loser]

        for jj in range(board_size):
            for kk in range(board_size):
                if board[jj,kk] == loser:
                    board[jj,kk] = winner

        if verbose:
            print()
            print(f"Round {round + 1}")
            print()
            print(board)

            print(f"Players remaining: {players}")

    # Add the overall game winner to the total
    game_winner = players[0]
    game_winners[game_winner - 1] += 1

# Normalize
print()
print("Total Game Winner by Square")
print()
print(f"game_winners = {game_winners}")
print()

for ii in range(9):
    print(f"{game_winners[ii]:4d}", end=" ")
    if np.mod(ii, board_size) == board_size-1:
        print()

norm_winners = game_winners / number_of_games
print()
print("Normalized Winner Squares")
print()
print(norm_winners)
print()

for ii in range(9):
    print(f"{norm_winners[ii]:0.4f}", end=" ")
    if np.mod(ii, board_size) == board_size-1:
        print()
print()